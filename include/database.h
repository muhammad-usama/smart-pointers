#include <memory>
#include <vector>
#include "student.h"

namespace emumba::training
{
    class database 
    {
        std::vector<emumba::training:: student> student_data_list;

        public:
            database();
            const std::shared_ptr<student> get_student_reference_shared(std::string student_name);
            std::unique_ptr<student> get_student_reference_unique(std::string student_name);
            std::vector<student> :: iterator get_student_iterator(std::string student_name);
    };
}
