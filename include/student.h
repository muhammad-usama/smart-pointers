#include <string>
#include <map>

namespace emumba::training
{
    class student 
    {
        private:
            struct student_record 
            {
                std::string name;
                std::string roll_no;
                int age;
                float cgpa;
            }student_info;
            std::map<std::string /*subject name*/, int /*marks*/> result;

        public:
            student();
            student(std::string name,std::string roll_no, int age, float cgpa);
            ~student();
            int get_subject_marks(std::string subject);
            void set_subject_marks(std::string subject, int marks);
            void print_all_marks();
            void print_student_data();
            // Setter Functions
            void set_name(std::string name);
            void set_rollno(std::string roll_no);
            void set_age(int age);
            void set_cgpa(float cgpa);
            // Getter Functions
            std::string get_name();
            std::string get_rollno();
            int get_age();
            float get_cgpa();      
    };
}
