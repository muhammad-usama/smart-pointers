#include <iostream>
#include "database.h"

using namespace std;
using namespace emumba::training;

template <typename T>
void print_student_record(T &student_ptr)
{
    if(student_ptr != NULL)
        student_ptr->print_student_data();
}

int main()
{
    database Student_Data;
    shared_ptr<student> Shared_ptr = Student_Data.get_student_reference_shared("Usama");
    print_student_record(Shared_ptr);
    unique_ptr<student> Unique_ptr = Student_Data.get_student_reference_unique("Student 1");
    print_student_record(Unique_ptr);
    return 0;
}
