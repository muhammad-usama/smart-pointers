#include "database.h"
using namespace emumba::training;

database::database()
{
    student s1("Student 1", "101", 21,3.1);
    student s2("Student 2", "102", 22,3.2);
    student s3("Student 3", "103", 23,3.3);
    student s4("Student 4", "104", 24,3.4);
    student s5("Student 5", "105", 25,3.5);
    student_data_list.push_back(s1);
    student_data_list.push_back(s2);
    student_data_list.push_back(s3);
    student_data_list.push_back(s4);
    student_data_list.push_back(s5);
}

std::vector<student> :: iterator database::get_student_iterator(std::string student_name)
{
    std::vector<student>::iterator iter;
    for (iter = student_data_list.begin(); iter != student_data_list.end(); iter++)
    {
        if (student_name == iter->get_name())
            return iter;  
    }
    return iter;
}

const std::shared_ptr<student> database::get_student_reference_shared(std::string student_name)
{
    std::vector<student>::iterator iter = get_student_iterator(student_name);
    if(iter != student_data_list.end())
        return std::make_shared<student>(*iter); 
    else
        return nullptr;
}

std::unique_ptr<student> database::get_student_reference_unique(std::string student_name)
{
    std::vector<student>::iterator iter = get_student_iterator(student_name);
    if(iter != student_data_list.end())
        return std::make_unique<student>(*iter); 
    else
        return nullptr;
}